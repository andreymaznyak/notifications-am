/**
 * Created by AndreyMaznyak on 11.04.2016.
 */
'use strict';
angular.module('andreymaznyak.notifications', [])
.service('notificationsService',function(){
        var notificationFunc;
        try{
            if (!!nw) {
                notificationFunc = notify_desktop;
            }
        }catch(e){
            console.log(e);
            try{
                if (!!TTS) {
                    notificationFunc = notify_telephone;
                }
            }catch(e){
                console.log(e);
                notificationFunc = notify_browser;
            }
        }

        this.notify = notificationFunc;
        var notifications = this.notifications = [];

        setInterval(notifyAll,20000);
        this.notifyAll = notifyAll;
        function notifyAll(){
            for(var i = 0; i < notifications.length; i++){
                notificationFunc(notifications[i]);
            }
        }

        function notify_desktop(data) {

            var notification = new Notification(data.title, data.options);
            notification.onclick = function () {
                console.log('clicked');
            };

            notification.onshow = function () {
                // play sound on show
                //var myAud = document.getElementById("audio1");
                //if (!!myAud)
                //    myAud.play();
                // auto close after 1 second
                //setTimeout(function () {
                //    notification.close();
                //}, 1000);
            };
            console.log(notification);
        }

        function notify_telephone(data) {
            TTS
                .speak({
                    text: data.title,
                    locale: 'ru-RU',
                    rate: 0.85
                }, function () {
                    console.log('success');
                }, function (reason) {
                    console.log(reason);
                });
        }

        function notify_browser(data) {
            alert(data.title + '\n' + data.options.body);
        }
});